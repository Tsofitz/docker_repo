"""
docker ex3 - This file parse url's given as an argument
"""

import sys
import requests


def main():
    url_list = sys.argv[1].split(",")
    for url in url_list:
        r = requests.get(url)
        print(url)
        print(r.content)


if __name__ == "__main__":
    main()