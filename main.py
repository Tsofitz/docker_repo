"""
Jenkins ex - This file calculate power of 2 for each number
"""

import sys


def main():
    number = int(sys.argv[1])
    for i in range(number):
        print("{} {}".format(i, i ** 2))


if __name__ == "__main__":
    main()